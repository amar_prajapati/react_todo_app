import React from 'react';
import './App.css';
import Layout from './components/Layout/Layout';
import img from './images/background_img.jpg';
import axios from 'axios';

function App() {
  return (
    <div className="layout">
      <img src={img} />
        <Layout>        
        </Layout>
    </div>
  );
}

function fetch_records(){
  let tasks = [];
  const url = "http://localhost:5000/tasks/tasks";
        axios.get(url)
        .then(response => {
          tasks['result'] =  Object.keys(response.data.result);
        })
        // console.log(tasks);
        return tasks;
}

export default App;
