import React from 'react';

const task = (props) => (
            <li className={(props.task_completed.status === false)? "list-group-item task_list strike_out": "list-group-item task_list" }>
                <label className="container_checkbox">
                    <input type="checkbox" onClick={props.check_click} defaultChecked={ (props.task_completed.status === false)? "checked": ""}/>
                    <span className="checkmark"></span>
                </label>
                <i onClick={props.click} className="fa fa-times remove_icon" aria-hidden="true"></i>
                {(props.task_completed.status === true) ?
                    <i onClick={props.imp_task} className={(props.task_completed.is_imp === false) ? "fa fa-star star_icon" : "fa fa-star star_icon_imp"} aria-hidden="true"></i> 
                    : ''
                }
                <p>{props.list}</p>
            </li>
);

export default task;