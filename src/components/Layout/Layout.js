import React from 'react';
import classes from './Layout.css';
import Task from '../Task/Task';
import { motion } from "framer-motion";
import axios from 'axios';

window.id = 0;
let task_lists = null;
let input_val = '';
class Layout extends React.Component{

    constructor() {
        super();
        this.state = {
            tasks : [],
            input: '',
            error: '',
            is_data_fetched: false
        }
    }

    componentDidMount(){
        const url = "http://localhost:5000/tasks/tasks";
        axios.get(url)
        .then(response => {
            this.setState({
                tasks: (response.data.result),
                is_data_fetched: true
            })
        })
    }

    onChange = (e) => {
        input_val = e.target.value;
        this.setState(
            {
                input: input_val,
                error: ''
            }
        );
    }
    
    addTask = (e) => {
        e.preventDefault();
        let tasks_arr = Object.keys(this.state.tasks);
        if(input_val === ''){
            this.setState({
                input: '',
                error: 'Enter the task.'
            });
        }else{
            const data = {
                name: this.state.input,
                status: 1,
                is_imp: 0
            }
            console.log(data);
            const url = "http://localhost:5000/tasks/add";
            axios.post(url, data)
            .then(response => {
                console.log(response);
                this.componentDidMount();
            }, (error) => {
                console.log(error);
            });
            this.setState({
                input: '',
                error: ''
            });

        }
        input_val = '';
    }

    deleteTask = (i) => {
        const url = "http://localhost:5000/tasks/delete/" + i;
        
        axios.delete(url)
        .then(response => {
            console.log(response);
            this.componentDidMount();
        }, (error) => {
            console.log(error);
        });
    }

    completedTask = (e, i) => {
        const task = this.state.tasks[i];
        let data;

        if(task.status === true){
            data = {
                status: 0,
                is_imp: 0
            }
        }else{
            data = {
                status: 1
            }
        }
        const url = "http://localhost:5000/tasks/update/" + task._id;
        
        axios.put(url, data)
        .then(response => {
            console.log(response);
            this.componentDidMount();
        }, (error) => {
            console.log(error);
        });
    }

    importantTask = (e, i) => {
        const task = this.state.tasks[i];
        let data;

        if(task.is_imp === true){
            data = {
                is_imp: 0
            }
        }else{
            data = {
                is_imp: 1
            }
        }
        const url = "http://localhost:5000/tasks/update/" + task._id;
        
        axios.put(url, data)
        .then(response => {
            console.log(response);
            this.componentDidMount();
        }, (error) => {
            console.log(error);
        });
    }

    render(){
        return(
            <center>
                <div className="col-md-4"></div>
                <div className="col-md-4">
                    <h1>Todos</h1>
                    <div className="card text-white bg-white col-md-3">
                        <div className="card-body">
                        <div className="tab-content tasks_tab">
                                <ul className="tab-pane active task_lists" id="all_tasks">
                                        {
                                            (this.state.is_data_fetched === false) ? '' :  
                                            Object.values(this.state.tasks).map( (task, i) => (
                                                    <Task 
                                                        id={task._id}
                                                        list={task.name}
                                                        click={() => this.deleteTask(task._id)}
                                                        check_click = {(e) => this.completedTask(e, i)}
                                                        imp_task = {(e) => this.importantTask(e, i)}
                                                        key={i}
                                                        task_completed={task}
                                                    ></Task>
                                                ))
                                        }
                                </ul>
                                <ul className="tab-pane task_lists" id="active_tasks">
                                    {
                                        (!this.state.is_data_fetched) ? '' : 
                                        Object.values(this.state.tasks).map( (task, i) =>
                                            {
                                                if(task.status === true)                                            
                                                    return <Task 
                                                        id={task._id}
                                                        list={task.name}
                                                        click={() => this.deleteTask(task._id)}
                                                        check_click = {(e) => this.completedTask(e, i)}
                                                        key={i}
                                                        task_completed={task}
                                                    ></Task>
                                            
                                            })
                                    }
                                </ul>
                                <ul className="tab-pane task_lists" id="completed_tasks">
                                    {
                                        (!this.state.is_data_fetched) ? '' :
                                        Object.values(this.state.tasks).map( (task, i) => 
                                            {
                                                if(task.status === false)                                            
                                                    return <Task 
                                                            id={task._id}
                                                            list={task.name}
                                                            click={() => this.deleteTask(task._id)}
                                                            check_click = {(e) => this.completedTask(e, i)}
                                                            key={i}
                                                            task_completed={task}
                                                        ></Task>
                                            
                                            })
                                    }
                                </ul>
                            </div>
                            <ul className="nav nav-tabs">
                                <li className="active"><a data-toggle="tab" href="#all_tasks">All ({Object.values(this.state.tasks).length})</a></li>
                                <li><a data-toggle="tab" href="#active_tasks">Active</a></li>
                                <li><a data-toggle="tab" href="#completed_tasks">Completed</a></li>
                                
                            </ul>
                            <div className="card-title">
                                <form onSubmit={this.addTask}>
                                    <input type="text" id="task" placeholder="Enter task" className="form-control input_field" aria-label="Default" aria-describedby="inputGroup-sizing-default" onChange={this.onChange} value={input_val} autoComplete="off"/>
                                </form>
                            </div>
                        </div>
                        {/* <p className="task_sepn">test</p> */}
                    </div>
                </div>
            </center>
        );
    }
}

export default Layout;