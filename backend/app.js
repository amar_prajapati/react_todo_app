const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const tasks = require('./routes/task.route');
const login = require('./routes/login.route');
const db = require('./db');
const jwt = require('jsonwebtoken');

const app = express();

db;

app.use(express.static(__dirname + "/public"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cors());
app.use('/tasks', tasks);
app.use('/user', login);


// app.get('/api', (req, res) => {
//     res.json({
//         msg: 'Welcome to API!'
//     });
// });

// app.post('/api/posts', verifyToken, (req, res) => {
//     jwt.verify(req.token, 'secretkey', (err, authData) => {
//         if(err){
//             res.sendStatus(403);
//         }else{
//             res.json({
//                 msg: 'Post Created...',
//                 authData
//             });
//         }
//     });
// });

// app.post('/api/login', (req, res) => {
//     const user = {
//         id: 1,
//         username: 'ap',
//         email: 'ap@gmail.com'
//     }

//     // jwt.sign({ user }, 'secretkey', { expiresIn: '30s' }, (err, token) => {
//     jwt.sign({ user }, 'secretkey', (err, token) => {
//         res.json({
//             token
//         });
//     });
// });

// // Verify token
// function verifyToken(req, res, next){
//     // Get auth header value
//     const bearerHeader = req.headers['authorization'];

//     if(bearerHeader !== undefined){
//         const bearer = bearerHeader.split(' ');
//         const bearerToken = bearer[1];
//         req.token = bearerToken;
//         next();
//     }else{
//         res.sendStatus(403);
//     }
// }


let port = 5000;

app.listen(port, () => {
    console.log('Server is up and running on port numner ' + port);
});
