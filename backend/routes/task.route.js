const express = require('express');
const jwt = require('jsonwebtoken');
const router = express.Router();

const task_controller = require('../controllers/task.controller');


router.post('/add', verifyToken, task_controller.add);
router.get('/tasks', verifyToken, task_controller.tasks);
router.put('/update/:id', verifyToken, task_controller.task_update);
router.delete('/delete/:id', verifyToken, task_controller.task_delete);

function verifyToken(req, res, next){
    // Get auth header value
    const bearerHeader = req.headers['authorization'];

    if(bearerHeader !== undefined){
        const bearer = bearerHeader.split(' ');
        const bearerToken = bearer[1];
        req.token = bearerToken;
        next();
    }else{
        res.sendStatus(403);
    }
}

module.exports = router;