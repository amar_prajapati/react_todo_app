const express = require('express');
const jwt = require('jsonwebtoken');
const router = express.Router();

const login_controller = require('../controllers/login.controller');

router.post('/login', login_controller.login);
router.post('/register', login_controller.register);

module.exports = router;