const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let UserSchema = new Schema({
    name: {type: String, required: true, max: 100},
    email: {type: String, unique: true, required: true, max: 100},
    password: {type: String, required: true, max: 100}
});

module.exports = mongoose.model('Users', UserSchema);
