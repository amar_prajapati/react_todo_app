const Task = require('../models/tasks.model');
const jwt = require('jsonwebtoken');

exports.add = function (req, res) {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if(err){
            res.sendStatus(403);
        }else{
            var task = new Task(
                {
                    name: req.body.name,
                    status: req.body.status,
                    is_imp: req.body.is_imp
                }
            );

            task.save(function (err) {
                if (err) {
                    return err;
                }
                res.send({msg: 'Task Added successfully!', user_data: authData})
            })
        }
    });
};

exports.tasks = function (req, res) {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if(err){
            res.sendStatus(403);
        }else{
            Task.find({}, function(err, tasks) {
                var tasks_list = {};
                var i = 0;
                tasks.forEach(function(task) {
                    tasks_list[i] = task;
                    i++;
                });
        
                res.send({'result': tasks_list, user_data: authData});  
            });
        }
    });
};

exports.task_update = function (req, res) {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if(err){
            res.sendStatus(403);
        }else{
            Task.findOneAndUpdate({_id: req.params.id}, req.body, function(err, task){
                if(err){
                    return err;
                }
                res.send({task: task, user_data: authData});
            });
        }
    });
};

exports.task_delete = function (req, res) {
    jwt.verify(req.token, 'secretkey', (err, authData) => {
        if(err){
            res.sendStatus(403);
        }else{
            Task.findByIdAndRemove(req.params.id, function(err){
                if(err){
                    return err;
                }
                res.send({msg: 'Task deleted successfully!', user_data: authData});
            });
        }
    });
};