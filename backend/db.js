const mongoose = require('mongoose');
const dbname = "task_list";
const url = "mongodb://localhost:27017";
const mongoOptions = {useNewUrlParser : true};

mongoose.connect(url+dbname, mongoOptions);

mongoose.Promise = global.Promise;

var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('Db connected!');
});

module.exports = db;